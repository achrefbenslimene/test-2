const textShortener = (text, length, separator) => {
  if (text == null) {
    return "";
  }
  if (text.length <= length) {
    return text;
  }
  return `${text.substr(0, length)}${separator || "..."}`;
};

export default textShortener;
