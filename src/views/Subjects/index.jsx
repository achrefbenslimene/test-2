import { useState } from "react";
import Logo from "../../components/Logo";
import Counter from "./Counter";
import List from "./List";
import Form from "./Form";
import { subjects as SUBJECTS } from "../../constants/subjects.json";

const Subjects = () => {
  const [subjects, setSubjects] = useState(SUBJECTS);

  return (
    <div className='subjects'>
      <Logo />
      <Counter subjectsLength={subjects.length} />
      <div className='layout'>
        <List subjects={subjects} deleteSubject={setSubjects} />
        <Form addSubject={setSubjects} />
      </div>
    </div>
  );
};

export default Subjects;
