import Card from "./Card";

const List = ({ subjects, deleteSubject }) => {
  if (subjects.length === 0) {
    return <div style={{ width: "100%" }}></div>;
  }

  return (
    <div className='list'>
      {subjects.map((subject) => (
        <Card
          subject={subject}
          deleteSubject={deleteSubject}
          key={subject.id}
        />
      ))}
    </div>
  );
};

export default List;
