const Counter = ({ subjectsLength }) => {
  const counter = (value) => {
    if (value === 0) {
      return "Il n'y a pas de matières.";
    }
    if (value === 1) {
      return "Il y a une seule matière.";
    }
    return `Il y a ${value} matières.`;
  };

  return <div className='counter'>{counter(subjectsLength)}</div>;
};

export default Counter;
