import React from "react";
import ReactDOM from "react-dom";
import "./main.scss";
import Subjects from "./views/Subjects";
import Modal from "./components/Modal";
import { ModalProvider } from "./contexts/ModalContext";
import reportWebVitals from "./reportWebVitals";

ReactDOM.render(
  <React.StrictMode>
    <ModalProvider>
      <Subjects />
      <Modal />
    </ModalProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
