import { createContext, useState } from "react";

const initialState = {
  isOpen: false,
  data: null,
};

const ModalContext = createContext({
  modal: initialState,
  showModal: () => {},
  closeModal: () => {},
});

export const ModalProvider = ({ children }) => {
  const [modal, setModal] = useState(initialState);

  const showModal = (data) => {
    setModal(() => {
      return {
        isOpen: true,
        data,
      };
    });
  };

  const closeModal = () => {
    setModal(() => {
      return initialState;
    });
  };

  return (
    <ModalContext.Provider
      value={{
        modal,
        showModal,
        closeModal,
      }}
    >
      {children}
    </ModalContext.Provider>
  );
};

export default ModalContext;
