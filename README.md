## Quick run

```bash
git clone git@gitlab.com:takiacademy-alternance/frontend-team/test-2.git test-2
cd test-2/
git checkout -b test2/myname-mylastname
git push
npm install
npm start
```

## Links

Runs the app in the development mode.<br />

- Demo: http://localhost:3000
